public class Racional {
    private int numerador;
    private int denominador;



    public Racional(){
        this.numerador = 1;
        this.denominador = 1;
    }

    public Racional(int a, int b) throws Exception {
        if(b == 0)
            throw new Exception("Denominador Cero");


        this.numerador = a;
        this.denominador = b;
       // simplificar();
    }

    public int getNumerador() {
        return numerador;
    }

    public int getDenominador() {
        return denominador;
    }
    public void setNumerador(int numerador) {
        this.numerador = numerador;
    }

    public void setDenominador(int denominador) throws Exception {
        if(denominador == 0)
            throw new Exception("Denominador Cero");
        this.denominador = denominador;
       // simplificar();
    }

    /**
     * a/b + c/d = (a*d + b*c) / b*d
     * @param r2
     * @return
     */
    public Racional sumar(Racional r2){
        try{
            return new Racional(this.numerador*r2.getDenominador() + this.denominador*r2.getNumerador(),
                                 this.denominador*r2.getDenominador());
            //simpiflicar();
        }catch (Exception ie){
            //Excepción que nunca se lanzará
        }
        return null;
    }
    public Racional resta(Racional r2){
        try{
            return new Racional(this.numerador*r2.getDenominador() - this.denominador*r2.getNumerador(),
                    this.denominador*r2.getDenominador());
            //simpiflicar();
        }catch (Exception ie){
            //Excepción que nunca se lanzará
        }
        return null;
    }

    public Racional multiplicacion(Racional r2){
        try{
            return new Racional(this.numerador * r2.getNumerador(), this.denominador * r2.getDenominador());
            //simplificar();
        }catch (Exception ie){
            //Excepción que nunca se lanzará
        }
        return null;
    }


    public Racional division(Racional r2) throws Exception{ //c/d
        if(r2.getNumerador() == 0) // c == 0 - excepción
            throw new Exception("Divisior vale cero");

        return this.multiplicacion(r2.inverso()) // a/b * inverso(c/d) --- a/b*d/c

    }

    public Racional inverso() throws Exception{
        if(this.numerador == 0)//no le puedo dar la vuelta porque daria indeterminació
            throw new Exception("Denominador cero");

        return new Racional(this.denominador, this.numerador);
    }


    public void simplificar(){
        int div = mcd(this.numerador, this.denominador);
        this.numerador /= div;
        this.denominador /= div; // this.denominador = this.denominador / div;

        if(this.denominador < 0 ){
            this.denominador *= -1;
            this.numerador *= -1;
        }
    }

    public String toString(){
        return this.numerador + "/" + this.denominador;
    }



}
