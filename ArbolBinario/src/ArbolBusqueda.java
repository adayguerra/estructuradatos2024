public class ArbolBusqueda {
    NodoBinario nodoRaiz;

    public ArbolBusqueda(NodoBinario nodoRaiz) {
        this.nodoRaiz = nodoRaiz;
    }
    public ArbolBusqueda() {
        this.nodoRaiz = null;
    }

    public boolean isEmpty(){
        return nodoRaiz == null;
    }

    public NodoBinario getNodoRaiz() throws Exception{
        if(isEmpty()){
            throw new Exception("El arbol no tiene nodo");
        }
        return nodoRaiz;
    }

    public ArbolBusqueda hijoIzq() throws Exception{
        if(isEmpty()){
            throw new Exception("El arbol no tiene nodo");

        }
        return new ArbolBusqueda(this.nodoRaiz.getNodoIzd());
    }
    public ArbolBusqueda hijoDer() throws Exception{
        if(isEmpty()){
            throw new Exception("El arbol no tiene nodo");

        }
        return new ArbolBusqueda(this.nodoRaiz.getNodoDer());
    }

    public NodoBinario buscar(int elemento) throws Exception{
        return buscar(elemento, this.nodoRaiz);
    }

    public NodoBinario  buscar(int elemento, NodoBinario<Integer> t) throws Exception{
        while(t != null)
            if(elemento < t.dato)
                t = t.getNodoIzd();
            else if (elemento > t.dato)
                t = t.getNodoDer();
            else
                return t;
        return t;
    }

    public void insertar(int elemento) throws Exception{
        nodoRaiz = insertar(elemento, nodoRaiz);
    }
    public NodoBinario insertar(int elemento, NodoBinario<Integer> t) throws Exception{

        if(t == null)
            t = new NodoBinario<>(elemento, null, null);
        else if(elemento < t.getDato())
            t.setNodoIzd(insertar(elemento, t.getNodoIzd()));
        else if(elemento > t.getDato())
            t.setNodoDer(insertar(elemento, t.getNodoDer()));
        else
            throw new Exception("El elemento ya existe");

        return t;
    }

}
