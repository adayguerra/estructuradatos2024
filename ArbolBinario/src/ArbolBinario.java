public class ArbolBinario<E> {
    NodoBinario<E> nodoRaiz;

    public ArbolBinario(){
        nodoRaiz = null;
    }

    public ArbolBinario(NodoBinario<E> nodoRaiz){
        this.nodoRaiz = nodoRaiz;
    }
    public boolean isEmpty(){
        return nodoRaiz == null;
    }

    public void empty(){
        nodoRaiz = null;
    }

    public NodoBinario<E> getNodoRaiz() throws Exception{
        if(isEmpty())
            throw new Exception("Raiz: Arbol vacio.");
        return nodoRaiz;
    }

    public ArbolBinario<E> hijoIzquierdo() throws Exception{
        if(isEmpty())
            throw new Exception("Error hijo derecho: Arbol vacio.");
        return new ArbolBinario<>(nodoRaiz.getNodoIzd());
    }
    public ArbolBinario<E> hijoDerecho() throws Exception{
        if(isEmpty())
            throw new Exception("Error hijo derecho: Arbol vacio.");
        return new ArbolBinario<>(nodoRaiz.getNodoDer());
    }


    public void addHijoIzquierdo(ArbolBinario<E> hijoIzquierdo) throws Exception{
        if(hijoIzquierdo == null)
            throw new Exception("Error addHijoIzquierdo: Hijo es un Arbol vacio.");
        if(isEmpty())
            throw new Exception("Error addHijoIzquierdo: Arbol vacio.");

        nodoRaiz.setNodoIzd(hijoIzquierdo.getNodoRaiz());
    }

    public void addHijoDerecho(ArbolBinario<E> hijoDerecho) throws Exception{
        if(hijoDerecho == null)
            throw new Exception("Error addHijoDerecho: Hijo es un Arbol vacio.");
        if(isEmpty())
            throw new Exception("Error addHijoDerecho: Arbol vacio.");

        nodoRaiz.setNodoDer(hijoDerecho.getNodoRaiz());
    }

    public void delete(ArbolBinario<E> subArbol) throws Exception{
        if(subArbol == null)
            throw new Exception("Error delete: Hijo es un Arbol vacio.");

        if(subArbol.nodoRaiz==nodoRaiz)
            this.nodoRaiz = null;

        else{
            ArbolBinario<E> parent = parent(subArbol);
            if(parent.isEmpty())
                throw new Exception("Error delete: Arbol vacio.");
            else {
                if(parent.getNodoRaiz().getNodoIzd() == subArbol.getNodoRaiz())
                    parent.getNodoRaiz().setNodoIzd(null);
                else
                    parent.getNodoRaiz().setNodoDer(null);
            }

        }

    }

    private ArbolBinario<E> parent(ArbolBinario<E> subArbol) throws Exception{
        if(subArbol == null)
            throw new Exception("Error addHijoDerecho: Hijo es un Arbol vacio.");
        if(isEmpty()|| subArbol.isEmpty() || subArbol.getNodoRaiz() == this.nodoRaiz )
            return new ArbolBinario<E>();
        else if(nodoRaiz.getNodoIzd() == subArbol.getNodoRaiz() || nodoRaiz.getNodoDer() == subArbol.getNodoRaiz())
            return this;
        else {
            ArbolBinario<E> p = (new ArbolBinario<E>(nodoRaiz.getNodoIzd())).parent(subArbol);
            if(p.isEmpty())
                return (new ArbolBinario<E>(nodoRaiz.getNodoDer())).parent(subArbol);
            else
                return p;
        }

    }

    public static void preorden(NodoBinario nodo){
        if(nodo != null){
            System.out.print(nodo.getDato());
            preorden(nodo.getNodoIzd());
            preorden(nodo.getNodoDer());
        }
    }

    public static void inorden(NodoBinario nodo){
        if(nodo != null){

            inorden(nodo.getNodoIzd());
            System.out.print(nodo.getDato());
            inorden(nodo.getNodoDer());
        }
    }

    public static void postorden(NodoBinario nodo){
        if(nodo != null){

            postorden(nodo.getNodoIzd());
            postorden(nodo.getNodoDer());
            System.out.print(nodo.getDato());
        }
    }

    public static void main(String[] args) {
        ArbolBinario<String> arbolA = new ArbolBinario<>(new NodoBinario<String>("A"));
        ArbolBinario<String> arbolB = new ArbolBinario<>(new NodoBinario<String>("B"));
        ArbolBinario<String> arbolC = new ArbolBinario<>(new NodoBinario<String>("C"));
        ArbolBinario<String> arbolD = new ArbolBinario<>(new NodoBinario<String>("D"));
        ArbolBinario<String> arbolE = new ArbolBinario<>(new NodoBinario<String>("E"));
        ArbolBinario<String> arbolF = new ArbolBinario<>(new NodoBinario<String>("F"));
        ArbolBinario<String> arbolG = new ArbolBinario<>(new NodoBinario<String>("G"));

        try {
            arbolA.addHijoIzquierdo(arbolB);

            arbolA.addHijoDerecho(arbolC);
            arbolB.addHijoIzquierdo(arbolD);

            arbolC.addHijoIzquierdo(arbolE);
            arbolC.addHijoDerecho(arbolF);

            arbolF.addHijoDerecho(arbolG);
            System.out.println("PRE");
            ArbolBinario.preorden(arbolA.getNodoRaiz());
            System.out.println("\nIN");
            ArbolBinario.inorden(arbolA.getNodoRaiz());
            System.out.println("\nPOST");
            ArbolBinario.postorden(arbolA.getNodoRaiz());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
