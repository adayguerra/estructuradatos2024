public class NodoBinario<E>  {
    E dato;
    NodoBinario<E> nodoIzd;
    NodoBinario<E> nodoDer;

    public NodoBinario(E dato) {
        this.dato = dato;
        this.nodoIzd = null;
        this.nodoDer = null;
    }

    public NodoBinario(E dato, NodoBinario<E> nodoIzd, NodoBinario<E> nodoDer) {
        this.dato = dato;
        this.nodoIzd = nodoIzd;
        this.nodoDer = nodoDer;
    }

    public E getDato() {
        return dato;
    }


    public NodoBinario<E> getNodoIzd() {
        return nodoIzd;
    }

    public void setNodoIzd(NodoBinario<E> nodoIzd) {
        this.nodoIzd = nodoIzd;
    }

    public NodoBinario<E> getNodoDer() {
        return nodoDer;
    }

    public void setNodoDer(NodoBinario<E> nodoDer) {
        this.nodoDer = nodoDer;
    }


}
