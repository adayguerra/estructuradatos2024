public class Suma {
    public static int suma(int a)
    {
        int suma=0;
        for (int i = 0; i <= a  ; i++) {
            suma = suma + i;
        }
        return suma;
    } // T(a) = tinitsuma + tiniti + a * (tcom + tcont + tsum) +›t


    public static int mayor(int []array){
        int mayor = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] > mayor) {
                mayor = array[i];
            }
        }// T(n) = tin + tiinic + N (tcom + tcont + tif) + tresul
        //T(n) = tin + tiinic + N (tcom + tcont + tif + tasig) + tresul
        return mayor;
    }
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
        long startTime = System.nanoTime();
        suma(100);
        long endTime = System.nanoTime();
        long totalTime = endTime - startTime;

        long startTime2 = System.nanoTime();
        suma(1000);
        long endTime2 = System.nanoTime();
        long totalTime2 = endTime2 - startTime2;

        long startTime3 = System.nanoTime();
        suma(10000);
        long endTime3 = System.nanoTime();
        long totalTime3 = endTime3 - startTime3;


        System.out.println("t1:"+totalTime+" t2:"+totalTime2+" t3:"+totalTime3);}




    }
}
