import complejidad.Cronometro;
import complejidad.MetodosPrueba;

public class Complejidad {




    public static void main(String[] args) {
        Cronometro cron = new Cronometro();
        MetodosPrueba metodos = new MetodosPrueba();

        for (int i = 10; i < 10000; i*=10) {
            cron.inicia();
            metodos.m2(i);
            cron.termina();
            System.out.println(cron.tiempo());
        }


    }

}
