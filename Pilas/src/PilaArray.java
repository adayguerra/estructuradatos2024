public class PilaArray <E>{
    private E[] pila;
    private int cima;

    public PilaArray(){
        pila = (E[]) new Object[10];
        cima = 0;
    }

    public void push(E e) throws DesbordamientoSuperior {
        if(cima >= pila.length){
            throw new DesbordamientoSuperior("Desbordamioento superior de la pila");
        }
        pila[cima++] = e;
    }

    public E pop() throws DesbordamientoInferior {
        if(cima == 0){
            throw new DesbordamientoInferior("Desbordamioento inferior de la pila");
        }

        return pila[--cima];
    }

    public E peek() throws DesbordamientoInferior {
        if(cima == 0){
            throw new DesbordamientoInferior("Desbordamioento inferior de la pila");
        }

        return pila[cima - 1];
    }

    public boolean isEmpty(){
        return cima == 0;
    }

    public void clear(){
        cima = 0;
    }

    public static void main(String[] args) {
        PilaArray <String> pila = new PilaArray<>();

        try {
            pila.push("1");
            pila.push("2");
            pila.push("3");
            pila.push("4");
            pila.push("5");
            System.out.println(pila.pop());
            System.out.println(pila.pop());
            System.out.println(pila.pop());
            pila.push("9");
            pila.push("9");
            pila.push("9");
            pila.push("9");
            pila.clear();
        } catch (DesbordamientoSuperior | DesbordamientoInferior e) {
            throw new RuntimeException(e);
        }
    }
}
