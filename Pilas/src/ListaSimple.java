public class ListaSimple<E> {
    NodoSimple<E> cabecera;

    public ListaSimple(){
        cabecera = new NodoSimple(null);

    }

    public boolean estaVacia(){
        return cabecera.siguiente == null;
    }

    public void insertarPrimero(E data){
        NodoSimple<E> aux = new NodoSimple<>(data);// este tiene como siguiente null es lo que ocurre en el constructor de nodosimple

        aux.siguiente = cabecera.siguiente;
        cabecera.siguiente = aux;
    }


    public void insertarUltimo(E data){
        NodoSimple<E> aux = new NodoSimple<>(data);// este tiene como siguiente null es lo que ocurre en el constructor de nodosimple

        NodoSimple<E> ultimo = cabecera;
        while(ultimo.siguiente != null)
            ultimo = ultimo.siguiente;

       ultimo.siguiente = aux;
    }

    public void insertarOrdenado(E data){
        NodoSimple<E> aux = new NodoSimple<>(data);
        NodoSimple<E> anterior = cabecera;

        while(anterior.siguiente != null &&  ((Comparable)anterior.siguiente.dato).compareTo(aux.dato) < 0 ){
           anterior = anterior.siguiente;
        }
        aux.siguiente = anterior.siguiente;
        anterior.siguiente = aux;
    }

    public int buscar(E data){
        NodoSimple<E> aux = new NodoSimple<>(data);
        int pos = 0;
        while(aux != null){
            if(aux.dato.equals(data)){
                return pos;
            }else
                aux = aux.siguiente;
            pos++;
        }
        return -1;
    }

    public boolean eliminar(E data){
        NodoSimple<E> anterior = cabecera;
        while(anterior.siguiente != null && !anterior.siguiente.dato.equals(data)){
            anterior = anterior.siguiente;
        }
        if(anterior.siguiente != null){
            anterior.siguiente = anterior.siguiente.siguiente;
            return true;
        }
        return false;
    }

    public void vaciar(){
        cabecera.siguiente = null;
    }

    @Override
    public String toString() {
        String result = "";
        NodoSimple<E> aux = cabecera;
        while(aux != null){
            result += (Object)aux + " [" + aux.dato + " "  + "siguiente: " + aux.siguiente + "]\n" ;
            aux = aux.siguiente;
        }
        return result;
    }

    public static void main(String[] args) {
        ListaSimple<Integer> miLista = new ListaSimple<>();
        miLista.insertarUltimo(1);
        miLista.insertarUltimo(2);
        miLista.insertarUltimo(3);
        miLista.insertarUltimo(4);
        miLista.insertarUltimo(5);
        System.out.println(miLista);
        miLista.eliminar(4);
        System.out.println(miLista);

        miLista.vaciar();
        miLista.insertarUltimo(1);
        miLista.insertarUltimo(2);
        miLista.insertarUltimo(3);
        System.out.println(miLista);
        miLista.insertarPrimero(0);
        System.out.println(miLista);
        miLista.insertarPrimero(-2);
        System.out.println(miLista);

        miLista.insertarOrdenado(-1);
        System.out.println(miLista);

    }


}
