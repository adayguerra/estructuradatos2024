import java.util.LinkedList;

public class PilaLista <E>{
   private ListaSimple<E> lista;


   public PilaLista(){
       lista = new ListaSimple<>();
   }

   public void push(E elemento){
       lista.insertarPrimero(elemento);
   }

   public E peek() throws DesbordamientoInferior {
       if(lista.estaVacia())
           throw new DesbordamientoInferior("Desbordamiento inferior de la pila");

       return lista.cabecera.siguiente.dato;
   }

    public E pop() throws DesbordamientoInferior {
        if(lista.estaVacia())
            throw new DesbordamientoInferior("Desbordamiento inferior de la pila");

        E obj = lista.cabecera.siguiente.dato;
        lista.eliminar(obj);
        return  obj;
    }

    public void clear(){
       lista.vaciar();
    }

    public boolean isEmpty(){
       return lista.estaVacia();
    }



}
