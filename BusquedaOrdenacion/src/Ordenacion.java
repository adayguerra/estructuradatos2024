import java.awt.image.AreaAveragingScaleFilter;
import java.util.Arrays;

public class Ordenacion {

    public static void insercion(int []a){
        int temp;
        int i,j;
        for(i = 1; i < a.length; i++){
            temp = a[i];
            for(j = i; j > 0 && temp < a[j-1]; j--){
                a[j] = a[j-1];
            }
            a[j] = temp;
            System.out.println(Arrays.toString(a));
        }
    }

    public static void inserccionBinaria(int a[]){
        int temp;
        int i,j;
        int inf, sup, medio;
        for(i = 1; i < a.length; i++) {
            temp = a[i];

            inf = 0;
            sup = i-1;
            while(inf <= sup){
                medio = (sup+inf)/2;
                if(a[medio] < temp){
                    inf = medio+1;
                }else{
                    sup = medio -1;
                }
            }
            for(j = i; (j > inf);j--){
                a[j] = a[j-1];
            }
            a[j] = temp;
            System.out.println(Arrays.toString(a));

        }
    }


    public static void seleccion(int[] a){
        int temp;
        int menor;
        int i,j;

        for(i = 0; i < a.length - 1; i++){
            menor = i;
            for(j = i + 1; j < a.length; j++){
                if(a[menor] > a[j]){
                    menor = j;
                }
            }

            temp = a[i];
            a[i] = a[menor];
            a[menor] = temp;
            System.out.println(Arrays.toString(a));
        }
    }

    public static void intercambioMejoradoo(int []a){
        int i,j;
        int temp;
        boolean hayIntercambio = true;
        for(i = 0; i < a.length - 1 && hayIntercambio ; i++){
            hayIntercambio = false;
            for(j = a.length - 1; j > i; j--){

                if(a[j-1]>a[j]){
                    hayIntercambio = true;
                    temp = a[j-1];
                    a[j-1] = a[j];
                    a[j] = temp;

                }

            } System.out.println(Arrays.toString(a));
        }
    }


    public static void vibracion(int []a) {
        int j;
        int temp;
        int posicionUltimoCambio, izquierda, derecha;
        boolean hayIntercambio = true;

        izquierda = 1;
        derecha = a.length - 1;
        posicionUltimoCambio = derecha;

        do {
            hayIntercambio = false;
            for (j = derecha; j >= izquierda; j--) {
                if (a[j - 1] > a[j]) {
                    temp = a[j - 1];
                    a[j - 1] = a[j];
                    a[j] = temp;
                    hayIntercambio = true;
                    posicionUltimoCambio = j;
                }
            }
            if (hayIntercambio) {
                System.out.println(Arrays.toString(a));
            }
            izquierda = posicionUltimoCambio + 1;
            for (j = izquierda; j <= derecha; j++) {
                if (a[j - 1] > a[j]) {
                    temp = a[j - 1];
                    a[j - 1] = a[j];
                    a[j] = temp;
                    hayIntercambio = true;
                    posicionUltimoCambio = j;
                }

            }
            if (hayIntercambio) {
                System.out.println(Arrays.toString(a));
            }
            derecha = posicionUltimoCambio - 1;
        } while (derecha > izquierda && hayIntercambio);
    }

    public static void shellShort(int []a){
        int intervalo, i, j, temp;

        for(intervalo = a.length/2; intervalo > 0; ){
            System.out.println( intervalo);
            System.out.println( "a" + Arrays.toString(a));
          for(i = intervalo; i<a.length; i++){
              temp = a[i];
              for(j = i; j >= intervalo && temp < a[j-intervalo]; j-=intervalo){
                a[j] = a[j-intervalo];
              }
              a[j] = temp;
          }
          if(intervalo == 2)
              intervalo = 1;
          else
              intervalo = (int)(intervalo/2.2);
            System.out.println(Arrays.toString(a));
        }

    }
    public static void intercambio(int []a){
        int i,j;
        int temp;

        for(i = 0; i < a.length - 1  ; i++){

            for(j = a.length - 1; j > i; j--){

                if(a[j-1]>a[j]){

                    temp = a[j-1];
                    a[j-1] = a[j];
                    a[j] = temp;

                }

            }System.out.println(Arrays.toString(a));
        }
    }

    public static void mergeSort(int []a){
        int[] vectorAux = new int[a.length];
        mergeSort(a, vectorAux, 0, a.length-1);
    }

    public static void  mergeSort(int []a, int[] vectorAux, int izq, int dcha){
        if(izq<dcha){
            int centro = (izq+dcha)/2;
            mergeSort(a, vectorAux, izq, centro);
            mergeSort(a, vectorAux, centro+1, dcha);
            System.out.println(Arrays.toString(a));
            merge(a, vectorAux, izq, centro+1, dcha);
        }
    }

    public static void quicksort(int []a){
        quicksort(a, 0, a.length-1);
    }

    public static void quicksort(int []a, int ini, int fin){

        int medio =  (ini+fin)/2;
        int pivote;
        int i,j;
        if(fin - ini >= 1){
            medio = (ini+fin)/2;
            if( a[medio] < a[ini])
                intercambiar(a,ini,medio);
            if(a[fin]<a[ini])
                intercambiar(a,ini,fin);
            if(a[fin] < a[medio])
                intercambiar(a, medio, fin);
        }

        intercambiar(a, medio, fin-1);
        pivote = a[fin-1];

        i = ini;
        j = fin -1;
        while(i < j){
            while(i < j && a[i] <= pivote){
                i++;
            }
            while(i < j &&  pivote <= a[j]){
                j--;
            }
            if(i<j ){
                intercambiar(a,i,j);
            }
        }
        intercambiar(a,i,fin-1);
        quicksort(a, ini, i -1);
        quicksort(a, i + 1, fin);
    }
    public static void merge(int []a, int[] vectorAux, int posIzq, int posDer, int posFin) {
        int i;
        int finIzq = posDer - 1;
        int posAux = posIzq;
        int numElementos = posFin - posIzq + 1;

        while ((posIzq <= finIzq) && (posDer <= posFin)) {
            if (a[posIzq] < a[posDer]) {
                vectorAux[posAux] = a[posIzq];
                posIzq++;
            } else {
                vectorAux[posAux] = a[posDer];
                posDer++;
            }
            posAux++;

        }

        while ((posIzq <= finIzq)) {
            vectorAux[posAux] = a[posIzq];
            posAux++;
            posIzq++;

        }
        while ((posDer <= posFin)) {
            vectorAux[posAux] = a[posDer];
            posAux++;
            posDer++;


        }
        for (i = 0; i < numElementos; i++) {
            a[posFin] = vectorAux[posFin];
            posFin--;
        }
    }

    public static void intercambiar(int []a, int origen, int destino){
        int temp = a[origen];
        a[origen] = a[destino];
        a[destino] = temp;

    }

    public static void main(String[] args) {
        int []a = {42,20,17,13,28,14,23,15};
        int []b = {20,14,2,34,4,6,74,8,90,17,13,28,15,23,16,42};
        System.out.println(Arrays.toString(a));
        quicksort(a);
        System.out.println(Arrays.toString(a));

    }
}
