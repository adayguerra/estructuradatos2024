public class Buscar {



    public int busquedaLineal(int []a, int b){
        for(int i=0;i<a.length;i++){ //n
            if(a[i]==b){//O(1)
                return i;//O(1)
            }
        }
        return -1;//O(1) + n (O(1)+O(1)) = O(1) + N(O(1)) = O(1) +O(N) = O(n)
    }//O(1)+


    public int busquedaBinaria(int []a, int b){
        int inf = 0, sup = a.length-1;
        int med;

        while(inf<=sup){
            med = (sup + inf)/2;
            if(a[med] == b){
                return med;
            }
            if(a[med] > b){
                sup = med - 1;
            }else{
                inf = med + 1;
            }
        }
        return -1;
    }
}
