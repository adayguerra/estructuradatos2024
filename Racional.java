public class Racional {
    private int numerador;
    private int denominador;



    public Racional(){
        this.numerador = 1;
        this.denominador = 1;
    }

    public Racional(int a, int b) throws Exception {
        if(b == 0)
            throw new Exception("Denominador Cero");


        this.numerador = a;
        this.denominador = b;
        // simplificar();
    }

    public int getNumerador() {
        return numerador;
    }

    public int getDenominador() {
        return denominador;
    }
    public void setNumerador(int numerador) {
        this.numerador = numerador;
    }

    public void setDenominador(int denominador) throws Exception {
        if(denominador == 0)
            throw new Exception("Denominador Cero");
        this.denominador = denominador;
        // simplificar();
    }

    /**
     * a/b + c/d = (a*d + b*c) / b*d
     * @param r2
     * @return
     */
    public Racional sumar(Racional r2){
        try{
            Racional a =  new Racional(this.numerador*r2.getDenominador() + this.denominador*r2.getNumerador(),
                    this.denominador*r2.getNumerador());

            a.simplificar();
            return a;

        }catch (Exception ie){
            //Excepción que nunca se lanzará
        }
        return null;
    }
    public Racional resta(Racional r2){
        try{
            Racional a =  new Racional(this.numerador*r2.getDenominador() - this.denominador*r2.getNumerador(),
                    this.denominador*r2.getNumerador());
            a.simplificar();
            return a;

        }catch (Exception ie){
            //Excepción que nunca se lanzará
        }
        return null;
    }

    public Racional multiplicacion(Racional r2){
        try{
             Racional a  = new Racional(this.numerador * r2.getNumerador(), this.denominador * r2.getDenominador());
            a.simplificar();
            return a;
        }catch (Exception ie){
            //Excepción que nunca se lanzará
        }
        return null;
    }


    public Racional division(Racional r2) throws Exception{ //c/d
        if(r2.getNumerador() == 0) // c == 0 - excepción
            throw new Exception("Divisior vale cero");

        Racional a =     this.multiplicacion(r2.inverso()); // a/b * inverso(c/d) --- a/b*d/c
        a.simplificar();
        return a;
    }

    public Racional inverso() throws Exception{
        if(this.numerador == 0)//no le puedo dar la vuelta porque daria indeterminació
            throw new Exception("Denominador cero");

        Racional a =  new Racional(this.denominador, this.numerador);
        a.simplificar();
        return a;

    }


    public void simplificar(){
        int div = mcd(this.numerador, this.denominador);
        this.numerador /= div;
        this.denominador /= div; // this.denominador = this.denominador / div;
            /// (10/10)/(20/10) -> 1/2
        if(this.denominador < 0 ){
            this.denominador *= -1;
            this.numerador *= -1;
        }
    }
//a = 10 b = 20  mcd=10 -->a 1 b 2
    private int mcd(int a, int b) {
        if(a == 0 || b == 0){
            return 1;
        }
        a = Math.abs(a); //Math.abs(-1) => 1 Math.abs(1) => 1
        b = Math.abs(b);

        while(a != b){
            if(a > b){
                a = a - b;
            } else {
                b = b - a;
            }
        }
        return a;
    }

    public String toString(){
        return this.numerador + "/" + this.denominador;
    }



    public Racional leer(String texto)throws Exception{ // texto = "6/8"
        int numerador;
        int denominador;
        if(texto == null){
            throw new Exception("Texto mal formado.");
        }
        String val[] = texto.split("/");
        if(val.length != 2){
            throw new Exception("Texto mal formado.");
        }

        try{
           denominador = Integer.parseInt(val[1]);
           if(denominador == 0){
               throw new Exception("Denominador cero");
           }
           numerador = Integer.parseInt(val[0]);
           Racional a =  new Racional(numerador,denominador);
           a.simplificar();
           return a;
        }catch (NumberFormatException e){
            throw new Exception("Texto mal formado.");
        }
    }


    public Racional(String texto)throws Exception{

        if(texto == null){
            throw new Exception("Texto mal formado.");
        }
        String val[] = texto.split("/");
        if(val.length != 2){
            throw new Exception("Texto mal formado.");
        }

        try{
            this.denominador = Integer.parseInt(val[1]);
            if(this.denominador == 0){
                throw new Exception("Denominador cero");
            }
            this.numerador = Integer.parseInt(val[0]);

        }catch (NumberFormatException e){
            throw new Exception("Texto mal formado.");
        }
        this.simplificar();
    }





}
