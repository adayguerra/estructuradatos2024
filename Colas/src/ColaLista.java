public class ColaLista <E>{
    public ListaSimple<E> lista;

    public ColaLista(){
        lista = new ListaSimple<>();
    }

    public void add(E elemento){
        lista.insertarUltimo(elemento);
    }

    public E poll() throws DesbordamientoInferior {
        if(lista.estaVacia())
            throw new DesbordamientoInferior("Desbordamiento inferior");
        E object = peek();
        lista.eliminar(object);
        return object;

    }


    public E peek() throws DesbordamientoInferior {
        if(lista.estaVacia())
            throw new DesbordamientoInferior("Desbordamiento inferior");
        return lista.cabecera.siguiente.dato;
    }


    public boolean isEmpty() {
        return lista.estaVacia();
    }

    public void clean(){
        lista.vaciar();
    }
}
