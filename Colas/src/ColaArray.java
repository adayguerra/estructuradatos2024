public class ColaArray <E>{
    private final int TAMANO = 10;
    private E[] cola;
    private int primero;
    private int numElementos;

    public ColaArray(){
        cola = (E[]) new Object[TAMANO];
        primero = 0;
        numElementos = 0;
    }

    public boolean isEmpty(){
        return numElementos == 0;
    }

    public void clear(){
        primero =0;
        numElementos = 0;
    }

    public void add(E elemento) throws DesbordamientoSuperior {
        if(numElementos > TAMANO){
            throw new DesbordamientoSuperior("Desbordamiento Superior");
        }
        cola[(primero+numElementos)%TAMANO] = elemento;
        numElementos++;
    }

    public E poll() throws DesbordamientoInferior {
        if(numElementos <= 0){
            throw new DesbordamientoInferior("Desbordamiento Inferior");
        }
        E obj = cola[primero];
        primero = (primero+1)%TAMANO;
        numElementos--;
        return obj;


    }

    public E peek() throws DesbordamientoInferior {
        if(numElementos <= 0){
            throw new DesbordamientoInferior("Desbordamiento Inferior");
        }

        return cola[primero];
    }

}
