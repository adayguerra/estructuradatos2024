package Simple;

public class NodoSimple<E> {
    public E dato;
    public NodoSimple<E> siguiente;

    public NodoSimple(E data){
        this.dato = data;
        this.siguiente = null;
    }

    
}
