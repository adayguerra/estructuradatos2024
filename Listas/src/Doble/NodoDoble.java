package Doble;

public class NodoDoble<E> {
    public E dato;
    public NodoDoble<E> siguiente;
    public NodoDoble<E> anterior;

    public NodoDoble(E data) {
        this.dato = data;
        this.siguiente = null;
        this.anterior = null;
    }
}
