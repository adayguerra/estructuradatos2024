package Doble;

import Simple.ListaSimple;
import Simple.NodoSimple;

import java.util.LinkedList;

public class ListaDoble<E> {
    private NodoDoble<E> cabecera;
    private NodoDoble<E> cola;
    LinkedList<String> a;

    public ListaDoble() {
        cabecera = new NodoDoble(null);
        cola = new NodoDoble(null);

        cabecera.siguiente = cola;
        cola.anterior = cabecera;
    }

    public boolean estaVacia() {
        return cabecera.siguiente == cola;
    }

    public void insertarPrimero(E data) {
        NodoDoble<E> aux = new NodoDoble(data);
        aux.siguiente = cabecera.siguiente;
        aux.anterior = cabecera;
        cabecera.siguiente = aux;
        aux.siguiente.anterior = aux;
    }

    public void insertarUltimo(E data) {
        NodoDoble<E> aux = new NodoDoble(data);
        aux.siguiente = cola;
        aux.anterior = cola.anterior;
        cola.anterior = aux;
        aux.anterior.siguiente = aux;
    }

    public void insertarOrdenado(E data) {
        NodoDoble<E> aux = new NodoDoble(data);
        NodoDoble<E> posicion = cabecera;

        while (posicion.siguiente != cola && ((Comparable)posicion.siguiente.dato).compareTo(aux.dato) < 0) {
            posicion = posicion.siguiente;
        }

        aux.siguiente = posicion.siguiente;
        aux.anterior = posicion;
        posicion.siguiente = aux;
        aux.siguiente.anterior = aux;
    }


    public boolean eliminar(E data){
        NodoDoble<E> aux = cabecera.siguiente;
        while (aux != cola && !aux.dato.equals(data)) {
            aux = aux.siguiente;
        }

        if(aux != cola){
            aux.anterior.siguiente = aux.siguiente;
            aux.siguiente.anterior = aux.anterior;
            return true;
        }
        return false;
    }

    public String toString() {
        String result = "";
        NodoDoble<E> aux = cabecera;
        while(aux != null){
            result += (Object)aux + " [" + aux.dato + " "  + "siguiente: " + aux.siguiente +  "anterior: " + aux.anterior + "]\n" ;
            aux = aux.siguiente;
        }
        return result;
    }
    public void vaciar(){
        cabecera.siguiente = cola;
        cola.anterior = cabecera;
    }

    public int buscar(E data){
        NodoDoble<E> aux = cabecera.siguiente;
        int pos = 0;
        while(aux != cola) {
            if (aux.dato.equals(data))
                return pos;
            aux = aux.siguiente;
            pos++;
        }
        return -1;
    }

    public static void main(String[] args) {
        ListaDoble<Integer> miLista = new ListaDoble<>();
        miLista.insertarUltimo(1);
        miLista.insertarUltimo(2);
        miLista.insertarUltimo(3);
        miLista.insertarUltimo(4);
        miLista.insertarUltimo(5);


        System.out.println(miLista);
        miLista.eliminar(4);
        System.out.println(miLista);

        miLista.vaciar();
        miLista.insertarUltimo(1);
        miLista.insertarUltimo(2);
        miLista.insertarUltimo(3);
        System.out.println(miLista);
        miLista.insertarPrimero(0);
        System.out.println(miLista);
        miLista.insertarPrimero(-2);
        System.out.println(miLista);

        miLista.insertarOrdenado(-1);
        System.out.println(miLista);

    }
}
