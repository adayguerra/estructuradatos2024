package EjemploMap;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;

public class EjemploHashMap {
    public static void main(String[] args) {
        Hashtable<String, Coche> hm = new Hashtable<String, Coche>();
        Hashtable<String, Integer> hm2 = new Hashtable<String, Integer>();

        Coche c1 = new Coche("Audi", "A7","2345-BGF",324144);
        Coche c2 = new Coche("Audi", "A4","6445-GGF",2323);
        Coche c3 = new Coche("Audi",  "A2","8274-LBB",111211);
        Coche c4 = new Coche("Seat", "León","9753-GHJ",11211);
        Coche c5 = new Coche("Seat", "León","8367-FDR",34456);
        Coche c6 = new Coche("Toyota", "Corolla","3967-KKK",555465);

        hm2.put("1",1);
        hm2.put("2",2);

        hm.put(c1.getMatricula(),c1);// Maps --> key value / clave(matricula) valor(c1 coche)
        hm.put(c2.getMatricula(),c2);
        hm.put(c3.getMatricula(),c3);
        hm.put(c4.getMatricula(),c4);
        hm.put(c5.getMatricula(),c5);
        hm.put(c6.getMatricula(),c6);

//        for(Map.Entry<String, Coche> a : hm.entrySet())
//            System.out.println("Clave:"+a.getKey() + ", valor:"+ a.getValue());

        System.out.println(hm.get("3967-KKK"));
    }

}
