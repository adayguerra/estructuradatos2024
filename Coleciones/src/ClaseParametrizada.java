import java.util.ArrayList;

public class ClaseParametrizada <T,U>{
    private T dato;
    private U clave;

    public ClaseParametrizada(T dato, U clave){
        this.dato = dato;
        this.clave = clave;
    }

    public T getDato() {
        return dato;
    }

    public U getClave() {
        return clave;
    }

    public void setDato(T dato) {
        this.dato = dato;
    }

    public void setClave(U clave) {
        this.clave = clave;
    }

    public static void main(String[] args) {
        ClaseParametrizada<String, Integer> ejemplo = new ClaseParametrizada<>("ejemplo",10);
        ClaseParametrizada<Double, Float> ejemplo2 = new ClaseParametrizada<>(10.0,10.0f);

        String nombre[] = new String[5];
        nombre[0] = "Aday0";
        nombre[1] = "Aday1";
        nombre[2] = "Aday2";
        nombre[3] = "Aday3";
        nombre[4] = "Aday4";

        for (int i = 0; i <nombre.length ; i++) {
          //  System.out.println(nombre[i]);
        }

        ArrayList<String> nombres = new ArrayList<>();
        nombres.add("Aday0");
        nombres.add("Aday1");
        nombres.add("Aday2");
        nombres.add("Aday3");
        nombres.add("Aday4");

        for(String n:nombres)
            System.out.println(n);







    }
}
