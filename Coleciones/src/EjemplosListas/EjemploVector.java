package EjemplosListas;

import java.util.Vector;

public class EjemploVector {

    public static void main(String[] args) {
        Vector<Integer> vector = new Vector();

        new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                vector.add(i);
                System.out.println("Añadido:"+i);
                try {
                            Thread.sleep(1);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }).start();

        new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
               if(!vector.isEmpty()){
                   System.out.println("Eliminado:"+ vector.remove(0));
                   try {
                       Thread.sleep(6);
                   } catch (InterruptedException e) {
                       throw new RuntimeException(e);
                   }
               }

            }
        }).start();
    }
}
