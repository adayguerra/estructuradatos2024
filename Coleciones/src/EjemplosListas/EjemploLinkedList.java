package EjemplosListas;

import java.util.LinkedList;

class Tarea{
    String descripcion;
    int prioridad;

    public Tarea(String descripcion, int prioridad){
        this.descripcion = descripcion;
        this.prioridad = prioridad;
    }

    @Override
    public String toString() {
        return "Tarea{" +
                "descripcion='" + descripcion + '\'' +
                ", prioridad=" + prioridad +
                '}';
    }
}
public class EjemploLinkedList {

    LinkedList<Tarea> tareas = new LinkedList<>();

    public void añadirTarea(Tarea nueva){
        if(tareas.isEmpty())
            tareas.add(nueva);
        else {
            boolean pos = false;
            for (int i = 0; i < tareas.size(); i++) {
                if(tareas.get(i).prioridad < nueva.prioridad) {
                    tareas.add(i, nueva);
                    pos = true;
                    break;
                }
            }
            if(!pos)
                tareas.addLast(nueva);
        }
    }

    public void ejecutarTarea(){
        if(!tareas.isEmpty()){
            Tarea tarea = tareas.poll();
            System.out.println("Trabajando en ello..."+tarea);
        }else System.out.printf("No hay mas tareas..");

    }


    public void mostrarTareas(){
        for(Tarea a : tareas)
            System.out.println(a);
    }


    public static void main(String[] args) {
        EjemploLinkedList listaEnlazada = new EjemploLinkedList();
        listaEnlazada.añadirTarea(new Tarea("Comprar pan",1));
        listaEnlazada.añadirTarea(new Tarea("Cortar pan",10));
        listaEnlazada.añadirTarea(new Tarea("Comer pan",8));
        listaEnlazada.añadirTarea(new Tarea("Cocinar",4));

        System.out.println("Tareas pendientes");
        listaEnlazada.mostrarTareas();

        System.out.println("Ejecutar");
        listaEnlazada.ejecutarTarea();
        listaEnlazada.ejecutarTarea();

        System.out.println("Tareas pendientes");
        listaEnlazada.mostrarTareas();


    }
}
