package EjemploSet;

import javax.swing.*;

public class Coche implements Comparable<Coche> {
    String marca;
    String modelo;
    String matricula;
    int numkilometros;

    public Coche(String marca, String modelo, String matricula, int numkilometros) {
        this.marca = marca;
        this.modelo = modelo;
        this.matricula = matricula;
        this.numkilometros = numkilometros;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public int getNumkilometros() {
        return numkilometros;
    }

    public void setNumkilometros(int numkilometros) {
        this.numkilometros = numkilometros;
    }

    @Override
    public String toString() {
        return "Coche{" +
                "marca='" + marca + '\'' +
                ", modelo='" + modelo + '\'' +
                ", matricula='" + matricula + '\'' +
                ", numkilometros=" + numkilometros +
                '}';
    }


    public int compareTo(Coche o) {

        return this.matricula.compareTo(o.getMatricula());    

    }
}
