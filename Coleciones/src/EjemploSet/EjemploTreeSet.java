package EjemploSet;

import java.util.TreeSet;

public class EjemploTreeSet {


    public static void main(String[] args) {
        TreeSet<Coche> conjuntoCoches = new TreeSet<>();
        Coche c1 = new Coche("Audi", "A7","2345-BGF",324144);
        Coche c2 = new Coche("Audi", "A4","6445-GGF",2323);
        Coche c3 = new Coche("Audi",  "A2","8274-LBB",111211);
        Coche c4 = new Coche("Seat", "León","9753-GHJ",11211);
        Coche c5 = new Coche("Seat", "León","8367-FDR",34456);
        Coche c6 = new Coche("Toyota", "Corolla","3967-KKK",555465);



        conjuntoCoches.add(c1);
        conjuntoCoches.add(c2);
        conjuntoCoches.add(c3);
        conjuntoCoches.add(c4);
        conjuntoCoches.add(c5);
        conjuntoCoches.add(c6);


        System.out.println("Listado de coches:");
        for( Coche c: conjuntoCoches)
            System.out.println(c);


        conjuntoCoches.remove(new Coche(null,null,"9753-GHJ",0));
        System.out.println("Listado de coches:");
        for( Coche c: conjuntoCoches)
            System.out.println(c);


        System.out.println(conjuntoCoches.getFirst());
    }

}
